<?php

namespace Drupal\media_iconpicker\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\media\MediaInterface;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class IconController.
 */
class IconController extends ControllerBase {

  /**
   * Render an icon.
   */
  public function view(MediaInterface $media) {
    $thumbnail = $media->get('thumbnail')->entity;
    if (!$thumbnail) {
      throw new NotFoundHttpException();
    }

    // Make an image response.
    $uri = $thumbnail->getFileUri();
    $data = file_get_contents($uri);
    if (!$data) {
      return '';
    }
    $type = $thumbnail->getMimeType();

    // Add cache dependency on the media item.
    $cacheMetadata = new CacheableMetadata();
    $cacheMetadata->setCacheTags(['media:' . $media->id()]);

    $response = new CacheableResponse();
    $response->addCacheableDependency($cacheMetadata);
    $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, basename($uri));
    $response->headers->set('Content-Disposition', $disposition);
    $response->headers->set('Content-Type', $type);
    $response->setContent($data);
    return $response;
  }

}
