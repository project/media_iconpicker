<?php

namespace Drupal\media_iconpicker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'media_iconpicker' widget.
 *
 * @FieldWidget(
 *   id = "media_iconpicker",
 *   module = "media_iconpicker",
 *   label = @Translation("Media iconpicker"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = FALSE,
 * )
 */
class MediaIconpicker extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getSetting('target_type') === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'select',
      '#options' => $this
        ->getOptions($items->getEntity()),
      '#default_value' => $this
        ->getSelectedOptions($items),
      '#multiple' => FALSE,
    ];
    $element['#attributes']['class'][] = 'media-iconpicker';
    $element['#attributes']['class'][] = 'chosen-disable';
    $element['#attached']['library'][] = 'media_iconpicker/widget';
    return ['target_id' => $element];
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {

      // Limit the settable options for the current user account.
      $options = $this->fieldDefinition
        ->getFieldStorageDefinition()
        ->getOptionsProvider($this->column, $entity)
        ->getSettableOptions(\Drupal::currentUser());

      // Add an empty option if the widget needs one.
      if ($empty_label = $this
        ->getEmptyLabel()) {
        $options = [
          '_none' => $empty_label,
        ] + $options;
      }

      array_walk_recursive($options, [
        $this,
        'addImageUrl',
      ]);

      // Options might be nested ("optgroups"). If the widget does not support
      // nested options, flatten the list.
      if (!$this
        ->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }
      $this->options = $options;
    }
    return $this->options;
  }

  /**
   * Add encoded images to labels.
   */
  protected function addImageUrl(&$label, $id) {
    $label = str_replace('|', '', $label);
    $this->sanitizeLabel($label);

    $url = '/media_iconpicker/view/' . $id;
    $label = $label . '|' . $url;
  }

}
