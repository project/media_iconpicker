Media iconpicker is an alternative to the media library widget for selecting icons wrapped by media entities. It's an inline selection widget based on the [Fontawesome Iconpicker](https://github.com/itsjavi/fontawesome-iconpicker).

This is an entity reference widget restricted to `media` entities. It is currently single-select (cardinality = 1). It's designed to work with SVG images (https://www.drupal.org/project/svg_image may be helpful) but any media item where the thumbnail may be redered at 25px (max dimension) should work.
