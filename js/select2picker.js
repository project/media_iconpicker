(function($) {
  function getItem(items, value) {
    var filtered = $.grep(items, function(item) {
      return (item.value == value);
    });
    if (filtered.length == 1) {
      return filtered[0].icon;
    }
    return false;
  }

  Drupal.behaviors.media_iconpicker = {
    attach: function (context, settings) {
      $('.media-iconpicker').once('picker-created').each(function() {
        var $select = $(this);
        var val = $select.val();
        var items = $select.find('option').map(function() {
          var $opt = $(this);
          var value = $opt.attr('value');
          var text = $opt.text().split('|');
          var icon, name;
          if (value == "_none") {
            name = '- None -';
            icon = '<span class="picker-item-name">' + name + '</span>';
          }
          else {
            if (text.length != 2) {
              return null;
            }
            name = text[0];
            icon = '<span class="picker-item-name">' + name + '</span><img alt="visual representation of ' + name + '" src="' + text[1] + '">';
          }
          return {
            'value': value,
            'name': name,
            'icon': icon
          }
        }).toArray();

        if (!items.length) {
          return;
        }

        var $picker = $('<div class="media-iconpicker__container"><div class="picker-launcher"></div></div>');
        $select.hide();
        $select.after($picker);
        var default_value = getItem(items, val);
        var picker_opts = {
          items: items,
          itemProperty: 'icon',
          hideOnSelect: true,
          component: '.picker-launcher',
          container: '.media-iconpicker__container',
          input: '.picker-launcher',
          selectedCustomClass: 'picker-item--selected',
          defaultValue: default_value,
          selected: default_value,
          placement: 'bottomLeft',
        };
        console.log(picker_opts);
        $picker.picker(picker_opts);
        $picker.find('.picker-launcher').html(default_value);

        $picker.on('pickerSetValue', function(el) {
          var $container = $(this);
          var id = el.pickerValue.value;
          var $select = $container.prev('.media-iconpicker');
          if (id) {
            $select.val(id).change();
          }
        });
      });
    }
  }
})(jQuery);